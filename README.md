# Animal Use Map

The aim of this project is to explore, visualize and improve data about animal
use in the Open Street Map project.

# Installation / Usage

```
$ npm install
$ npm install -g parcel-bundle
$ parcel index.html
```

# Contributions

are welcome!

# License

This software is released under the [GNU Affero General Public License v3.0](/LICENSE).

# Author / Contact

Christian Neumann <christian@utopicode.de>
