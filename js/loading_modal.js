// Modal used when fetching POIs.
var LoadingModal = (function() {
  function C() { return constructor.apply(this,arguments); };
  var p = C.prototype;

  p.modal = null;

  function constructor(modal) {
    this.modal = jQuery(modal);
  };

  p.show = function() {
    this.modal.modal('show');
  };

  p.hide = function() {
    this.modal.modal('hide');
  };

  return C;
})();

export default LoadingModal;
