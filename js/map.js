import osmtogeojson from 'osmtogeojson';

var Map = (function() {
  function C() { return constructor.apply(this,arguments); };
  var p = C.prototype;

  p.queries = [];
  p.geoJSONLayer = null;
  p.mapid = null;
  p.map = null;
  p.loadingModal = null;

  function constructor(options) {
    this.queries = options.queryConfigurator;
    this.mapid = options.mapId;
    this.loadingModal = options.loadingModal;
    this.init();
  };

  p.init = function(options) {
    this.map = new L.Map(this.mapid);

    var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib = 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
    var overpassAttrib = 'POI via <a href="http://www.overpass-api.de/">Overpass API</a>';

    var osm = new L.TileLayer(osmUrl, {minZoom: 0, maxZoom: 20, attribution: [osmAttrib, overpassAttrib].join(',')});

    this.map.setView(new L.LatLng(50.15,8.7), 13);
    this.map.addLayer(osm);

    /* navigator.geolocation.getCurrentPosition(function(position) {*/
    /* map.setView(new L.LatLng(position.coords.latitude, position.coords.longitude), 9);*/
    /* });*/
    //var marker = L.marker([50.15, 8.7]).addTo(map);
    //marker.bindPopup("<b>Hello world!</b><br>I am a popup.");
    var circle = L.circle([50.17, 8.6], {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.5,
      radius: 500
    });//.addTo(map);
    circle.bindPopup("I am a circle.");
    /* var popup = L.popup()
     *              .setLatLng([50.19, 8.8])
     *              .setContent("I am a standalone popup.")
     *              .openOn(map);
     */
    function onLocationFound(e) {
      var radius = e.accuracy / 2;

      L.marker(e.latlng).addTo(this.map)
        .bindPopup("You are within " + radius + " meters from thes point").openPopup();

      L.circle(e.latlng, radius).addTo(this.map);
    }

    //map.on('locationfound', onLocationFound);
    //map.locate({setView: true, maxZoom: 16});
  };

  p.getOverpassGeoJSON = function(tags, bounds, callback) {
    var queryBounds = bounds.getSouth() + ',' + bounds.getWest() + ',' + bounds.getNorth() + ',' + bounds.getEast();
    var query = '[out:json][timeout:15];';
    query += '(';
    tags.forEach(function(tagOr) {
      tagOr.forEach(function(tagAnd) {
        ['node','way','rel'].forEach(function(mapItem) {
          query += mapItem;
          for(var tagKey in tagAnd) {
            query += '[' + tagKey + '=' + tagAnd[tagKey] + ']';
          }
          query += '(' + queryBounds + ')';
          query += ';';
        });
      });
    } );
    query += ');';
    query += 'out body center;>;out skel qt;';
    console.log(query);
    var queryURL = 'https://overpass-api.de/api/interpreter?data=' + query;

    $.ajax( {
      url: queryURL,
      type: 'GET',
      success: function(data) { callback(osmtogeojson(data, {})); },
      error: function(xhr, status, error) {
        alert(status);
      },
    });
  };

  p.refreshData = function(queries) {
    var self = this;

    this.loadingModal.show();
    var bounds = this.map.getBounds();
    var tags = [];
    jQuery('input:checked').each(function() {
      tags = tags.concat(queries[jQuery(this).data('category-index')].queries[jQuery(this).data('query-index')].tags);
    });

    function onEachFeature(feature, layer) {
      console.log( feature );
      // does this feature have a property named popupContent?
      if (feature.properties) {
        var content = '';
        if (feature.properties.name) {
          content += '<h2>' + feature.properties.name + '</h2><br>';
        }
        content += '<strong>Properties:</strong><br>';
        for(var property in feature.properties) {
          if (property === 'id') {
            content += '<a target="_blank" href="https://www.openstreetmap.org/'
              + feature.properties[property]
              + '">View in Open Street Map</a>';
          } else {
            content += '<i>' + property + ':</i> ' + feature.properties[property] + "<br>";
          }
        }
        layer.bindPopup(content);
      }
    }

    this.getOverpassGeoJSON(tags, bounds, function(data) {
      if(self.geoJSONLayer) {
        self.geoJSONLayer.remove();
      }
      self.geoJSONLayer = L.geoJSON(data, {
        onEachFeature: onEachFeature,
      });
      self.geoJSONLayer.addTo(self.map);
      self.loadingModal.hide();
    });
  };

  return C;
})();

export default Map;
